<?php

class Psixotherapeia_Theme {

	private static $_instance;

	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	private function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ), 15 );

		add_filter( 'astra_logo', array( $this, 'astra_logo' ) );
	}

	public function enqueue_styles() {
		$prefix  = defined( 'WP_DEBUG' ) && WP_DEBUG ? '' : '.min';
		$version = $this->_get_file_version( PSIXOTHERAPEIA_THEME_PATH . '/style' . $prefix . '.css' );
		wp_enqueue_style( 'psixotherapeia-theme', PSIXOTHERAPEIA_THEME_URL . '/style' . $prefix . '.css', array( 'astra-theme-css' ), $version, 'all' );
	}

	public function astra_logo( $html ) {
		ob_start();
		?>
        <a href="<?php echo esc_url( home_url() ); ?>" class="psixo-logo-text">
            <div class="psixo-logo-text__name"><?php _e( 'Ιωάννα Κουτσοπούλου', 'psixotherapeia' ); ?></div>
            <div class="psixo-logo-text__job"><?php _e( 'Ψυχολόγος - Ψυχοθεραπεύτρια', 'psixotherapeia' ); ?></div>
        </a>
		<?php
		$html .= ob_get_clean();

		return $html;
	}

	private function _get_file_version( $filename ) {

		$filetime = file_exists( $filename ) ? filemtime( $filename ) : '';

		return PSIXOTHERAPEIA_THEME_VERSION . ( ! empty( $filetime ) ? '-' . $filetime : '' );
	}
}