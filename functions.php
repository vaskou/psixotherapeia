<?php

define( 'PSIXOTHERAPEIA_THEME_VERSION', '1.0.1' );
define( 'PSIXOTHERAPEIA_THEME_PATH', get_stylesheet_directory() );
define( 'PSIXOTHERAPEIA_THEME_URL', get_stylesheet_directory_uri() );

include 'includes/class-psixotherapeia-theme.php';

Psixotherapeia_Theme::instance();