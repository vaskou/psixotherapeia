export const config = {
    // Project options
    projectURL: 'https://dev.psixotherapeia.gr/',
    browser: 'chrome',

    // Style options
    outputStyle: 'expanded', // Available options → 'compact' or 'compressed' or 'nested' or 'expanded'

    cssDestination: './',
    jsDestination: './assets/js/',
}